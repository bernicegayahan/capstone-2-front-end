//console.log("linked")
let adminControls = document.querySelector('#adminButton')
//target the container from the html document
let container = document.querySelector('#coursesContainer')

//lets determine if there is a user currently logged in
//lets capture one of the properties that is currently stored in our weg storage
const isAdmin = localStorage.getItem("isAdmin")
//lets create a control structure to determine the display in the front end
if (isAdmin == "false" || !isAdmin) {
    adminControls.innerHTML = null;
} else {
    adminControls.innerHTML = `
      <div class="col-md-2 offset-md-10">
        <a href="../pages/addCourse.html" class="btn btn-block btn-success">Add Courses</a>
      </div>
    `
}

//send a request to retrieve all documents from the courses collection
fetch('https://calm-refuge-36590.herokuapp.com/api/courses/').then(res => res.json()).then(jsonData => {
    console.log(jsonData) //we only inserted this as a checker

    //lets declare a variable that will display the result in the browser depending on the return.
    let courseData;

    //create a control structure that will determine the value that the variable will hold.
    if (jsonData.length < 1) {
        console.log("No Courses Available")
        courseData = "No Courses Available"
        container.innerHTML = courseData
    } else {
        //if the condition given is not met, display the contents of the array inside our page.

        //we will iterate the courses collection and display each course inside the browser
        courseData = jsonData.map(course => {
            //lets check the make up/structure of each element inside the courses collection
            console.log(course._id)
            console.log(course.name)
            //use template literals to pass/inject the properties of our object inside each section of a card component
            //what if i want to pass the value of a property from the 
            let cardFooter;

            if (isAdmin == "false" || !isAdmin) {
                cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
                `
            }

            else {
                cardFooter =
                    `
                   <a href="" class="btn btn-warning text-white btn-block">Edit Course</a>
                   <a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
                 `
            }

            //lets fix the current behavior of our courses page that it can only display the last object inserted inside the array.
            //so far ww did not indicate what will be the return of our map()
            return (
                `
                  <div class="col-md-6 my-3">
                      <div class="card">
                          <div class="card-body">
                              <h3 class="card-title">Course Name: ${course.name}</h3>
                              <p class="card-text text-left">Price: ${course.price}</p>
                              <p class="card-text text-left">Description: ${course.description}</p>
                              <p class="card-text text-left">Created On: ${course.createdOn}</p>
                          </div>
                          <div class="card-footer">
                              ${cardFooter}
                          </div>
                      </div>
                  </div>
                `
            )//think of a better message for this
        }).join("")//we will use this join() to create a return of a new string.
        //it concatenated all the objects inside the array and converted each into a string data type.

        container.innerHTML = courseData;
    }
})
