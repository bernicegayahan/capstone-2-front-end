//console.log("hello from js")
//get the value of the access token inside the local storage and place it inside a new variable
let token = localStorage.getItem("token");
console.log(token)

let lalagyan = document.querySelector("#profileContainer")

fetch('https://calm-refuge-36590.herokuapp.com/api/users/details', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
}).then(res => res.json())
    .then(jsonData => {
        //lets create a checker to make sure the fetch is successful

        //how are we going to display the information inside the front end component?
        //lets target the div element first using its id attribute
        lalagyan.innerHTML =
            `
			<div class="col-md-13">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name: ${jsonData.firstName}</h3>
					<h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
					<h3 class="text-center">Email: ${jsonData.email}</h3>
					<h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>
					<table class="table">
						<tr>
							<th>Name:</th>
							<th>Description:</th>
							<th>Enrolled on:</th>
							<th>Status: </th>
							<tbody id="courseBody"></tbody>
						</tr>
					</table>
				</section>
			</div>
	`

        for (let i = 0; i < jsonData.enrollments.length; i++) {
            fetch(`https://calm-refuge-36590.herokuapp.com/api/courses/${jsonData.enrollments[i].courseId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
                .then(res => res.json())
                .then(courseData => {
                    document.getElementById('courseBody').innerHTML +=
                        `
				<tr>
					<td>${courseData.name}</td>
					<td>${courseData.description}</td>
					<td> ${jsonData.enrollments[i].enrolledOn}</td>
					<td> ${jsonData.enrollments[i].status}</td>
				</tr>
			`
                });
        }
    })
