//console.log("hello log out")

//how to delete info/data inside the local storage
localStorage.clear();

//the clear method will wipe out/remove the contents of the local storage

//upon clearing our the local storage redirect the user back to the login page

window.location.replace("./login.html")
